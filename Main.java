package zprogramowanie_poziom_1.w4;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    static List<Human> humanList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int input;
        do {
            printInstruction();
            System.out.println();
            System.out.println("please make your choice");
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    addHuman();
                    break;
                case 2:
                    removeHuman();
                    break;
                case 3:
                    isEquals();
                    break;
                case 4:
                    printArray(humanList);
                    break;
            }
        } while (input != 5);
    }

    public static void addHuman() {

        System.out.println("Please give your age");
        int age = 0;
        age = getAge(age);
        scanner.nextLine();
        System.out.println("please give your name");
        String name = scanner.nextLine();
        System.out.println("Please give your Surname ");
        String surname = scanner.nextLine();
        Human human = new Human(age, name, surname);
        humanList.add(human);
    }

    private static int getAge(int age) {
        boolean isCorrect = false;
        do {
            try {
                age = scanner.nextInt();
                isCorrect = true;
            } catch (InputMismatchException e) {
                System.out.println(e);
                scanner.next();
            }
        } while (!isCorrect);
        return age;
    }

    public static void removeHuman() {
        System.out.println("Do you want remove human");
        String choose = scanner.nextLine();
        if (choose.equals("y")) {
            System.out.println("which index you want remove?");
            int index = scanner.nextInt();
            humanList.remove(index);
        } else if (choose.equals("n")) {
            System.out.println("thank you for give chance for us");
        }
    }

    public static void isEquals() {
        System.out.println("Which index do you want compare");
        System.out.println("first human");
        int firstChoose = scanner.nextInt();
        Human firstHuman = humanList.get(firstChoose);
        System.out.println("second human");
        int secondChoose = scanner.nextInt();
        Human secondHuman = humanList.get(secondChoose);
        if (firstHuman.equals(secondHuman)) {
            System.out.println("guys are twins");
        } else {
            System.out.println("they are different");
        }
    }

    public static void printInstruction() {
        System.out.println("1 - Add human" + " \n" + "2 - Remove human" + "\n" + "3 - Check they are queals" + "\n" + "4 - Print Humans" + "\n" + "5 - Exit");
    }

    public static void printArray(List<Human> arrayList) {
        for (Human v : arrayList) {
            System.out.println(v + "  index = " + arrayList.indexOf(v));
        }
    }
}
