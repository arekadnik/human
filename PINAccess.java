package zprogramowanie_poziom_1.w4;

import java.util.Scanner;

public class PINAccess {
    public static void main(String[] args) {
        enterPin();
    }

    public static void enterPin() {
        Scanner scanner = new Scanner(System.in);
        String pin = "";
        int count = 0;
        do {
            System.out.println("Please enter your PIN");
            pin = scanner.next();
             if (!(pin.equals(PIN.getPIN()))) {
                System.out.println("please write again PIN code");
            } else {
                System.out.println("welcome in your account");
                break;
            }
            count++;
        } while (count != 3);
        if (count == 3) {
            System.out.println("your account is BLOCKED!");
        }
    }

}

