package zprogramowanie_poziom_1.w4;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class MapTest {
    public static void main(String[] args) {

        Map<Double, Double> map = new TreeMap<>();
        for (double key = -10; key < 10; key += 0.5) {
            double value = Math.pow(key, 2);
            map.put(key, value);

        }
        for (Map.Entry<Double,Double> v : map.entrySet()){
            System.out.println(String.format("%6.2f",v.getKey()) + " = " + String.format("%-6.2f",v.getValue()));

        }
    }
}
